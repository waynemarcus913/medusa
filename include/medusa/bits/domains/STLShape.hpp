#ifndef MEDUSA_BITS_DOMAINS_STLSHAPE_HPP_
#define MEDUSA_BITS_DOMAINS_STLSHAPE_HPP_

#include <medusa/Config.hpp>
#include "STLShape_fwd.hpp"
#include <map>
#include <medusa/bits/spatial_search/KDTreeMutable.hpp>
#include "DomainDiscretization_fwd.hpp"
#include "discretization_helpers.hpp"
#include "PolygonShape.hpp"
#include "GeneralFill.hpp"
#include "BasicRelax.hpp"
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <medusa/bits/spatial_search/KDGrid.hpp>

/**
 * @file
 * Implementation of STL shapes.
 */

namespace mm {

template <typename vec_t>
STLShape<vec_t>::STLShape(const std::vector<STL::Triangle>& stl) {
    assert_msg(!stl.empty(), "Triangle list should not be empty.");
    int num_triangles = stl.size();
    std::map<vec_t, int> index;
    int num_points = 0;
    faces_.resize(num_triangles);
    normals_.resize(num_triangles);
    bbox_.first = bbox_.second = v(stl[0].p1);
    for (int i = 0; i < num_triangles; ++i) {
        const auto& t = stl[i];

        auto p1 = v(t.p1);
        if (index.count(p1) == 0) index[p1] = num_points++;
        faces_[i][0] = index[p1];
        bbox_.first = bbox_.first.cwiseMin(p1);
        bbox_.second = bbox_.second.cwiseMax(p1);

        auto p2 = v(t.p2);
        if (index.count(p2) == 0) index[p2] = num_points++;
        faces_[i][1] = index[p2];
        bbox_.first = bbox_.first.cwiseMin(p2);
        bbox_.second = bbox_.second.cwiseMax(p2);

        auto p3 = v(t.p3);
        if (index.count(p3) == 0) index[p3] = num_points++;
        faces_[i][2] = index[p3];
        bbox_.first = bbox_.first.cwiseMin(p3);
        bbox_.second = bbox_.second.cwiseMax(p3);

        normals_[i] = v(t.normal).normalized();
    }

    vertices_.resize(num_points);
    for (const auto& p : index) {
        vertices_[p.second] = p.first;
    }
}

template <typename vec_t>
DomainDiscretization<vec_t>
STLShape<vec_t>::discretizeBoundaryWithDensity(
        const std::function<scalar_t(vec_t)>& dx, int type) const {
    if (type == 0) type = -1;
    DomainDiscretization<vec_t> d(*this);

    int num_faces = faces_.size();
    std::vector<vec_t> v_normals(vertices_.size());
    for (int i = 0; i < num_faces; ++i) {
        for (int j = 0; j < 3; ++j) v_normals[faces_[i][j]] += normals_[i];
    }

    // vertices
    KDTreeMutable<vec_t> tree;
    for (int i = 0; i < num_faces; ++i) {
        int v1 = faces_[i][0];
        int v2 = faces_[i][1];
        int v3 = faces_[i][2];

        auto p1 = vertices_[v1];
        auto p2 = vertices_[v2];
        auto p3 = vertices_[v3];

        auto n = normals_[i];
        vec_t zunit = {0, 0, 1};
        scalar_t c = n.dot(zunit);
        if (c < 0) {
            zunit = -zunit;
            c = -c;
        }
        auto vp = n.cross(zunit);

        // Rotate to flat
        Eigen::Matrix<scalar_t, dim, dim> R; R.setIdentity();
        Eigen::Matrix<scalar_t, dim, dim> VP;
        VP << 0, -vp[2], vp[1], vp[2], 0, -vp[0], -vp[1], vp[0], 0;
        R += VP + 1/(1+c)*VP*VP;

        auto np1 = R*p1;
        auto np2 = R*p2;
        auto np3 = R*p3;
        scalar_t z = 1.0/3.0*(np1[2] + np2[2] + np3[2]);

        std::vector<Vec2d> pts = {np1.template head<2>(), np2.template head<2>(),
                                  np3.template head<2>()};
        PolygonShape<Vec2d> shape(pts);
        auto new_dx = [&](const Vec2d& p) { return dx(R.transpose()*vec_t(p[0], p[1], z)); };
        auto tri = shape.discretizeBoundaryWithDensity(new_dx);
        GeneralFill<Vec2d> fill; fill.seed(0); fill.numSamples(30).proximityTolerance(0.95);
        fill(tri, new_dx);

        for (const auto& p : tri.positions()) {
            vec_t g = R.transpose()*vec_t(p[0], p[1], z);
            if (tree.existsPointInSphere(g, dx(g))) continue;
            tree.insert(g);
            d.addBoundaryNode(g, type, normals_[i]);
        }
    }


    return d;
}

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_STLSHAPE_HPP_
